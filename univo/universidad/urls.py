from django.urls import path
from . import views
urlpatterns = [
    path('', views.home, name="home"),
    path('alumnos-list/', views.alumnosList, name="alumnos-list")
     
]
