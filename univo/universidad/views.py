from django.shortcuts import render
from django.http import JsonResponse

from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import Alumnoserializer

from .models import Alumno

@api_view(['GET'])
def home(request):
    api_urls = {
        'List': '/alumnos-list/',
        'Detail View': '/alumno-detail/<str:pk>/',
        'Create' : '/alumno-crear',
        'Update' :'/alumno-actualizar/<str:pk>',
        'Delete': '/alumno-eliminar/<str:pk>',
    }
    return Response(api_urls) 
@api_view(['GET'])
def alumnosList(request):
    alumnos = Alumno.objects.all()
    serializer = Alumnoserializer(alumnos, many=True)
    return Response(serializer.data)