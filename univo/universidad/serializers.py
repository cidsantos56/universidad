from rest_framework import serializers
from .models import Alumno

class Alumnoserializer(serializers.ModelSerializer):
    class Meta:
        model = Alumno
        fields = '__all__'